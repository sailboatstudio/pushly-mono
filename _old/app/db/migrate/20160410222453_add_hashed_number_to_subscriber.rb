class AddHashedNumberToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :phone_number_hashed, :string
  end
end

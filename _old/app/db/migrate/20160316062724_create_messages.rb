class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :sent_by
      t.text :message
      t.string :to
      t.string :from
      t.references :campaign, index: true, foreign_key: true
      t.references :account, index: true, foreign_key: true
      t.references :subscriber, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

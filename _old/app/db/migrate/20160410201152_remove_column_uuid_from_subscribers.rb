class RemoveColumnUuidFromSubscribers < ActiveRecord::Migration
  def change
    remove_column :subscribers, :uuid, :string
  end
end

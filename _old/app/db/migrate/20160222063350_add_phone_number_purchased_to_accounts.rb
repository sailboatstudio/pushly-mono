class AddPhoneNumberPurchasedToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :phone_number_purchased, :boolean
  end
end

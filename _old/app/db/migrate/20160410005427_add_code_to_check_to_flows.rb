class AddCodeToCheckToFlows < ActiveRecord::Migration
  def change
    add_column :flows, :code_to_check, :string
  end
end

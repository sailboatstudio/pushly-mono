class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.references :subscriber, index: true, foreign_key: true
      t.references :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

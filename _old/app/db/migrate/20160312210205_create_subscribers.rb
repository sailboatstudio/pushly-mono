class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.string :first_name
      t.string :last_name
      t.text :bio
      t.string :phone_number
      t.string :uuid

      t.timestamps null: false
    end
    add_index :subscribers, :phone_number, unique: true
    add_index :subscribers, :uuid, unique: true
  end
end

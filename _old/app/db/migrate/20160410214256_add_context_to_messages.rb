class AddContextToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :context, :string
  end
end

class RemoveHashFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :hash, :string
  end
end

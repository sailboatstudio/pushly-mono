class AddHiddenToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :hidden, :boolean, default: false
  end
end

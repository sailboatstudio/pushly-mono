class AddManageLinkToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :manage_token, :string
    add_column :subscribers, :manage_token_created_at, :datetime
  end
end

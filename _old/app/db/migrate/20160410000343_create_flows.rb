class CreateFlows < ActiveRecord::Migration
  def change
    create_table :flows do |t|
      t.string :name
      t.text :description
      t.integer :type
      t.integer :order
      t.string :trigger
      t.string :code_to_execute
      t.string :created_by
      t.integer :created_by_id

      t.timestamps null: false
    end
  end
end

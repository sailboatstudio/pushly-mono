class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.references :account, index: true, foreign_key: true
      t.integer :created_by
      t.string :name
      t.text :description
      t.text :message

      t.timestamps null: false
    end
  end
end

class RemoveCreatedByFromCampaign < ActiveRecord::Migration
  def change
    remove_column :campaigns, :created_by, :string
  end
end

class AddStatusToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :status, :integer, default: 0
  end
end

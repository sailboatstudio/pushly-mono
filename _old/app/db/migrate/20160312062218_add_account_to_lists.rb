class AddAccountToLists < ActiveRecord::Migration
  def change
    add_reference :lists, :account, index: true, foreign_key: true
  end
end

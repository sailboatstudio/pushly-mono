class RemoveNameColumsnFromSubscribers < ActiveRecord::Migration
  def change
    remove_column :subscribers, :first_name, :string
    remove_column :subscribers, :last_name, :string
  end
end

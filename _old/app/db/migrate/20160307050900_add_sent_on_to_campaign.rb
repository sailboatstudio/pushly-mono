class AddSentOnToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :sent_on, :datetime
  end
end

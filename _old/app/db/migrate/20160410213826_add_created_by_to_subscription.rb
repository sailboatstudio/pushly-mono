class AddCreatedByToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :created_by, :string
  end
end

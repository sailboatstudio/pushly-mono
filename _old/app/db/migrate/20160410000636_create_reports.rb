class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.string :phone_number
      t.string :email
      t.text :description
      t.references :subscriber, index: true, foreign_key: true
      t.references :list, index: true, foreign_key: true
      t.references :account, index: true, foreign_key: true
      t.integer :status

      t.timestamps null: false
    end
  end
end

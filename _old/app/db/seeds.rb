u = User.create_with(username: "iamjbecker", first_name: "Jeremy", last_name: "Becker", password: "123@qweqwe", password_confirmation: "123@qweqwe", phone_number: "2508641438").find_or_create_by(email: "me@iamjbecker.com")

u.confirmed_at = Time.now
u.save!

puts "User: " << u.username

ROLES = ['super', 'manager', 'subscriber']

ROLES.each do |r|
  Role.find_or_create_by(name: r)
  puts "Role: " << r
end

u.add_role :super

a = Account.create_with(name: "test", description: "The account for Jeremy's Personal Brand", friendly_name: "pushly-7ffe02f642-test", phone_number: "+17786545470", phone_number_purchased: true, user_id: u.id).find_or_create_by(sid: "ACcbcacf98fbab1ba6e5574cd7ddbec290")

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160410222453) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.string   "friendly_name"
    t.string   "sid"
    t.text     "description"
    t.string   "phone_number"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "phone_number_purchased"
    t.string   "uuid"
    t.string   "slug"
  end

  add_index "accounts", ["slug"], name: "index_accounts_on_slug", unique: true, using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "campaigns", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "name"
    t.text     "description"
    t.text     "message"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "sent_on"
    t.integer  "list_id"
  end

  add_index "campaigns", ["account_id"], name: "index_campaigns_on_account_id", using: :btree
  add_index "campaigns", ["list_id"], name: "index_campaigns_on_list_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "subscriber_id"
    t.integer  "account_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "status",        default: 0
  end

  add_index "conversations", ["account_id"], name: "index_conversations_on_account_id", using: :btree
  add_index "conversations", ["subscriber_id"], name: "index_conversations_on_subscriber_id", using: :btree

  create_table "flows", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "type"
    t.integer  "order"
    t.string   "trigger"
    t.string   "code_to_execute"
    t.string   "created_by"
    t.integer  "created_by_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "code_to_check"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "lists", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "account_id"
  end

  add_index "lists", ["account_id"], name: "index_lists_on_account_id", using: :btree

  create_table "lists_users", force: :cascade do |t|
    t.integer  "list_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lists_users", ["list_id"], name: "index_lists_users_on_list_id", using: :btree
  add_index "lists_users", ["user_id"], name: "index_lists_users_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.string   "sent_by"
    t.text     "message"
    t.string   "to"
    t.string   "from"
    t.integer  "campaign_id"
    t.integer  "account_id"
    t.integer  "subscriber_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "conversation_id"
    t.string   "sid"
    t.boolean  "hidden",          default: false
    t.string   "context"
  end

  add_index "messages", ["account_id"], name: "index_messages_on_account_id", using: :btree
  add_index "messages", ["campaign_id"], name: "index_messages_on_campaign_id", using: :btree
  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["subscriber_id"], name: "index_messages_on_subscriber_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "email"
    t.text     "description"
    t.integer  "subscriber_id"
    t.integer  "list_id"
    t.integer  "account_id"
    t.integer  "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "reports", ["account_id"], name: "index_reports_on_account_id", using: :btree
  add_index "reports", ["list_id"], name: "index_reports_on_list_id", using: :btree
  add_index "reports", ["subscriber_id"], name: "index_reports_on_subscriber_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "subscribers", force: :cascade do |t|
    t.text     "bio"
    t.string   "phone_number"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "manage_token"
    t.datetime "manage_token_created_at"
    t.integer  "status",                  default: 0
    t.string   "phone_number_hashed"
  end

  add_index "subscribers", ["phone_number"], name: "index_subscribers_on_phone_number", unique: true, using: :btree

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "list_id"
    t.integer  "subscriber_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "active",        default: false
    t.string   "created_by"
  end

  add_index "subscriptions", ["list_id"], name: "index_subscriptions_on_list_id", using: :btree
  add_index "subscriptions", ["subscriber_id"], name: "index_subscriptions_on_subscriber_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "phone_number"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "accounts", "users"
  add_foreign_key "campaigns", "accounts"
  add_foreign_key "campaigns", "lists"
  add_foreign_key "conversations", "accounts"
  add_foreign_key "conversations", "subscribers"
  add_foreign_key "lists", "accounts"
  add_foreign_key "lists_users", "lists"
  add_foreign_key "lists_users", "users"
  add_foreign_key "messages", "accounts"
  add_foreign_key "messages", "campaigns"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "subscribers"
  add_foreign_key "reports", "accounts"
  add_foreign_key "reports", "lists"
  add_foreign_key "reports", "subscribers"
  add_foreign_key "subscriptions", "lists"
  add_foreign_key "subscriptions", "subscribers"
end

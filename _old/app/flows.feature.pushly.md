# Pushly

## Flows

The Goal:
  `Have keywords people text to an account's # to accomplish a task.`
   This is done using a Flow.

Flow:
  `Something that is added to an account, a list, a campaign, or is System Wide. It has a unique Keyword to accomplish a task`
  Every flow *must* have a triggering Keyword and must accomplish an action.

  Flows are:
    |- Flow keywords are unique to each account.
    |- There are reserved system-wide namespaces.
    |- There are default un-changeable Flows created for each list.


  eg. Keywords for System Wide Accounts
    |- `UNSUB listname` -> Removes a user's subscription to the list
    |- `LISTINFO listname` -> Return's List's description, and Account Name
    |- `MANAGE` -> Returns a link for a user to manage all Pushly SUBSCRIPTIONS

  eg. User Defined Flows
    |- `dailydeals`
    |-
    |-

  **System Flows** *These flows never reach account owner, but bypass it entirely and go straight to Pushly.*
    |- `MANAGE` -> Returns a unique link for users to manage their account at Pushly and their subscriptions.
    |- `REPORT` -> Sends a report message to Pushly Admin. Reports don't go to the account's that sent them.
    |- `IS_SUBSCRIBER?` -> Check if number is a subscriber
    |- `NEW_SUBSCRIBER` -> Creates a new subscriber.
    |- `IS_ACCOUNT?` -> Checks if the # sent_to is an account or not.

  **Account Flows** *These flows are at an account level. They let a user interact with an account and it's lists.*
    |- `UNSUB` -> Maps to MANAGE for now.
    |- `SUB` ->
        |- `` -> Creates a subscription to the account's default list, unless subscriptions already exists.
        |- `listname` -> Creates a subscription to the list selected by the account.


Flow = {
  name:string
  description:text
  type:integer (0:system, 1:account, 2:list, 3:campaign)
  trigger:string
  actions:collection (Action model)
  code_to_execute:string
  created_by: string, (Model name)
  created_by_id: integer
}

<!-- Action = {
  type: integer (0:if, 1:elseif, 2:else)

} -->










## To Do

- [x] Edit messages#incoming method.
- [ ] Create Show/Edit for subscribers via unique expiring key
- [ ] Create Reporting view Text & WebForm

MANAGE : 3
REPORT : 4

New message comes in

### SYSTEM REQUESTS ###
  -> Is the incoming # a Pushly Subscriber?
      |- `YES` -> Find and return their account info. Set the message subscriber_id.
          |-> What is their account status?
              |- `0` -> *Message: Hi! What's your name?*
              |- `1` -> **PART TWO**
              |- `2` -> *Message: Your phone number is currently blocked from interacting with Pushly businesses. Want to re-enable it? Follow this link: _unique-link_*
      |- `NO` ->
          |-> Create a subscriber account.
          |-> Set Subscriber status to '0' : Incomplete
          |-> Subscribe them to Pushly's subscribers list.
          |-> *Message: Hi! What's your name?*
  -> Find the account the message is sent to.
      |-> Set the message account_id.

**PART TWO**
  -> Is the message a MANAGE request?
      |- `YES` -> Generate and send back a manage link.
      |- `NO` -> Skip
  -> Is the message a REPORT request?
      |- `YES` ->
          |-> Initiate a new Report.
          |-> From Pushly Account: *Message: Hi, we're Pushly.co. We are the system that _account-name_ uses for text-marketing. You indicated you wanted to report this company or a message they sent. If this was a mistake, reply 'CANCEL'. If not, please give us more information here: _unique-web-link_. Your privacy is important to us. Details are outlined on the website. Thanks!*
      |- `NO` -> Skip
  -> Is the message a INFO request?
      |- `YES` ->
          |-> *Message: Pushly is SMS for Small Businesses. You can text: MANAGE to update your account and what you receive from Pushly and our users.*
      |- `NO` -> Skip
  -> Is the message a SUB request?
      |- `YES` -> Is the second word a list keyword?
          |- `YES` -> Subscribe them to that list.
          |- `NO` -> Skip
      |- `NO` -> Skip
  -> Is the message a UNSUB request?
      |- `YES` -> Is the second word a list keyword?
          |- `YES` -> unsubscribe them to that list
              |-> *Message: You're unsubscribed from _list-name_. To further manage your account, click here: _manage-url_. Link is valid for one hour*
          |- `NO` -> Skip
      |- `NO` -> Skip


  -> Are they subscribed to the Account's General list?
      |- `YES` -> Skip
      |- `NO` -> Subscribe them to it.
          |-> *Message: `Hey _name_, thanks for subscribing to the _account-name_'s _list-name_. You've subscribed to a different business that uses Pushly in the past. To manage your account and subscriptions text MANAGE.`*


  -> Set the messages subscriber_id.
  -> Find or create Conversation.
      -> Add message to conversation





After Name Flow
  |-> Activate General subscription



























```ruby
rails g scaffold Account name:string twilio_name:string twilio_id:string description:text phone_number:string uuid:string:uniq user:references

rails g scaffold List name:string description:text account:references

rails g model PersonStatus name:string description:text

rails g scaffold Person name:string phone_number:string phone_number_hashed:string

rails g model PersonPersonStatus person:references person_status:references

rails g model SubscriptionStatus name:string description:text

rails g scaffold Subscription person:references list:references created_by:string

rails g model SubscriptionSubscriptionStatus subscription:references subscription_status:references

rails g scaffold Conversation subscription:references account:references person:references

rails g scaffold Campaign name:string message:text sent_at:datetime list:references account:references

rails g scaffold Message message:text to:string from:string author:string campaign:references conversation:references person:references account:references twilio_id:string


```
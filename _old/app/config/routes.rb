Rails.application.routes.draw do
  resources :reports
  resources :subscribers
  resources :subscriptions

  get 'accounts', to: 'accounts#index'
  post 'accounts', to: 'accounts#create'
  post 'messages/incoming', to: 'messages#incoming'
  post 'quicksend', to: 'messages#quicksend_create'

  get 'm/:phone_hash/:manage_token', to: 'subscribers#manage'

  resources :accounts, only: [:show, :new, :edit, :create, :update, :destroy], path: 'a' do
    get 'quicksend', to: 'messages#new'
    post 'quicksend', to: 'messages#quicksend_create'

    resources :lists
    resources :conversations, only: [:index, :show]
    resources :campaigns do
      post 'send_it', on: :collection
    end
  end

  if Rails.env.production?
    devise_for :users, :controllers => { :registrations => "registrations" }
  else
    devise_for :users
  end

  resources :users
  resources :roles

  root to: 'accounts#index'
  post '/md5', to: 'application#generate_md5'
  get '/md5', to: 'application#generate_md5'
end

require 'twilio-ruby'

Twilio.configure do |config|
  config.account_sid = ENV['twilio_account_sid']
  config.auth_token  = ENV['twilio_auth_token']
end

@twilio = Twilio::REST::Client.new

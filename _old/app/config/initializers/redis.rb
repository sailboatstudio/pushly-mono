if Rails.env.production?
  $redis = Redis.new(url: ENV['PUSHLY_REDIS_URL'])
else
  $redis = Redis.new
end

module ApplicationHelper

  def nav_link(link_text, icon, link_path)
    class_name = current_page?(link_path) ? 'active' : nil

    if icon === ""
      link_text = link_text
    else
      link_text = "<i class='icon #{icon}'></i>".html_safe + link_text
    end

    content_tag(:li, :class => class_name) do
      link_to link_path do
        link_text
      end
    end
  end

  def convolist_item(conversation, account)
    link_path = account_conversation_path(@account,conversation)
    active = current_page?(link_path) ? " active" : ""
    class_name = 'c-convolist_item' + active.to_s

    content_tag(:li, :class => class_name) do
      link_to link_path do
        content_tag(:span, conversation.subscriber.name, class: 'name') +
        # content_tag(:span, conversation.subscriber.phone_number, class: 'number') +
        content_tag(:span, conversation.messages.last.message, class: 'message')
      end
    end
  end

  def gravatar(email)
    email = email.strip.downcase
    hash = Digest::MD5.hexdigest(email)
    "<img src='//gravatar.com/avatar/#{hash}?s=184' class='avatar'>".html_safe
  end
end

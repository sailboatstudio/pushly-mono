class Campaign < ActiveRecord::Base
  resourcify
  belongs_to :account

  validates :name, presence: true
  validates :message, presence: true, allow_blank: true
  validates :account_id, presence: true

  def send_it
    if self.sent? === false
      list = List.find(self.list_id)
      subscribers = list.subscribers
      account = self.account

      subscribers.each do |subscriber|
        Message.create(
          sent_by: "account",
          account_id: account.id,
          subscriber_id: subscriber.id,
          campaign_id: self.id,
          to: subscriber.phone_number,
          from: account.phone_number,
          message: self.message
        )
      end

      self.sent_on = Time.now
      self.save!
    else
      return "This has already been sent!"
    end
  end

  def sent?
    self.sent_on ? true : false
  end
end

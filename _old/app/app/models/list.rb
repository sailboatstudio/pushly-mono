class List < ActiveRecord::Base
  resourcify

  belongs_to :account
  has_many :subscriptions
  has_many :subscribers, through: :subscriptions
  
  validates :name, presence: true
  validates :account_id, presence: true
end

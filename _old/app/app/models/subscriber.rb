class Subscriber < ActiveRecord::Base
  has_many :subscriptions
  has_many :lists, through: :subscriptions
  has_many :converastions

  validates :phone_number, presence: true, uniqueness: true

  after_create :generate_phone_number_hashed

  def send_account_blocked_message
    @subscriber = self
    @pushly = Account.where(sid: ENV['PUSHLY_SID'])

    Message.create({
      sent_by: "account",
      from: @pushly.phone_number,
      to: @subscriber.phone_number,
      account_id: @pushly.id,
      subscriber_id: @subscriber.id,
      text: "Your phone number is currently blocked from interacting with Pushly businesses. Want to re-enable it? Email us: hello@pushly.co"
    })
  end

  def generate_manage_token
    self.manage_token_created_at = Time.now
    self.manage_token = SecureRandom.urlsafe_base64(64)
    self.save!
    return self.manage_token
  end

  def check_manage_token(token_attempt)
    if Time.now <= self.manage_token_created_at + 1.hour && self.manage_token === token_attempt
      true
    else
      false
    end
  end

  def generate_manage_link
    phone_hash = self.phone_number_hashed
    manage_token = self.generate_manage_token
    url = 'http://localhost:3000' + '/m/' + phone_hash + '/' + manage_token

    return url
  end

  def send_manage_text
  end

  def send_info_text
  end

  def send_sub_text
  end

  def send_unsub_text
  end

  protected

  def generate_phone_number_hashed
    self.phone_number_hashed = Digest::MD5.hexdigest(self.phone_number)
    self.save!
  end

  # def engage_with_subscriber
  #   @client = Twilio::REST::Client.new
  #
  #   @client.account.messages.create({
  #     :from => Account.first.phone_number,
  #     :to => self.phone_number,
  #     :body => 'Welcome! What\'s your name?',
  #   })
  # end
end

class Conversation < ActiveRecord::Base
  resourcify

  belongs_to :account
  belongs_to :subscriber
  has_many :messages

  validates :subscriber_id, presence: true
  validates :account_id, presence: true

  # Status are as follows:
  #   0 = open
  #   1 = archived
  #   2 = blocked
  scope :available, -> { where(status: 0) }


  def archive
    self.set_status(1)
  end

  def unarchive
    self.set_status(0)
  end

  def block
    self.set_status(2)
  end

  def unblock
    self.set_status(0)
  end

  def set_status(status)
    self.status = status
    self.save
  end
end

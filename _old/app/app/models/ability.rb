class Ability
  include CanCan::Ability

  def initialize(user)
    user = user || User.new # for guest
    user_account_ids = user.accounts.pluck(:id)
    user_accounts_list_ids = user_account_ids.map { |id| Account.find(id).lists.map{ |l| l.id } }[0]

    if user.has_role? :super
      can :manage, :all
    elsif user.has_role? :manager
      can :manage, Account, :user_id => user.id
      can :create, Account
      can :manage, Campaign, :account_id => user_account_ids
      can :send_it, Campaign, :account_id => user_account_ids
      can :create, Campaign
      can :manage, List, :account_id => user_account_ids
      can :create, List
      can :manage, Subscription, :list_id => user_accounts_list_ids
      can :create, Subscription
      can :create, Subscriber
      can :read, Subscriber
      can :read, Conversation, :account_id => user_account_ids
      can :create, Conversation
    else
      can :read, Campaign
    end
  end
end

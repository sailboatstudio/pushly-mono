class User < ActiveRecord::Base
  rolify

  has_many :accounts
  after_create :set_default_role
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable

  validates :first_name,
    presence: true
  validates :last_name,
    presence: true
  validates :username,
    presence: true
  validates :phone_number,
    presence: { message: 'hello world, bad operation!' },
    length:   { minimum: 10, maximum: 15 },
    numericality: true


  def name
    first_name + " " + last_name
  end

  private

  def set_default_role
    add_role "manager"
  end
end

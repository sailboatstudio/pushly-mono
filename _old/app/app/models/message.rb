class Message < ActiveRecord::Base
  before_create :add_to_conversation
  after_create :send?
  after_create :push_to_the_world
  after_create :unarchive_converastion

  resourcify

  belongs_to :campaign
  belongs_to :account
  belongs_to :subscriber
  belongs_to :conversation

  validates :sent_by, presence: true
  validates :message, presence: true
  validates :hidden, presence: true
  # validates :conversation_id, presence: true

  def check_context(respond=false)
    text = self.message.split(" ")

    case text[0].upcase
    when "MANAGE"
      if respond
        respond_to_context(text[0])
      end
      self.hidden = true
      return "MANAGE"

    when "REPORT"
      if respond
        respond_to_context(text[0])
      end
      self.hidden = true
      return "REPORT"

    when "INFO"
      if respond
        respond_to_context(text[0])
      end
      self.hidden = true
      return "INFO"

    when "SUB"
      if respond
        respond_to_context(text[0])
      end
      self.hidden = true
      return "SUB"

    when "UNSUB"
      if respond
        respond_to_context(text[0])
      end
      self.hidden = true
      return "UNSUB"
    else
      return "NO_CONTEXT"
    end
  end

  def respond_to_context(context)
    # case context.uppcase
    # when "MANAGE"
    # when "REPORT"
    # when "INFO"
    # when "SUB"
    # when "UNSUB"
    # when "NO_CONTEXT"
    # end
  end

  protected

  def add_to_conversation
    convo = Conversation.find_or_create_by(
      subscriber_id: self.subscriber_id,
      account_id:    self.account_id
    )
    convo.touch(:updated_at)

    self.conversation_id = convo.id
  end

  def unarchive_converastion
    if self.conversation.status === 1
      Conversation.find(self.conversation_id).unarchive
    end
  end

  def send?
    if self.sent_by === "account"
      self.send_it
    end
  end

  def send_it
    account = self.account
    client = Twilio::REST::Client.new account.sid, account.auth_token

    client.messages.create(
      to:   self.to,
      from: self.from,
      body: self.message
    )
  end

  def push_to_the_world
    unless self.hidden
      id_hash = Digest::MD5.hexdigest(self.conversation_id.to_s)
      $redis.publish 'messages:new', { room: id_hash, message: self }.to_json
    end
  end
end

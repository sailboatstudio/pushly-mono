class Subscription < ActiveRecord::Base
  belongs_to :list
  belongs_to :subscriber

  validates :list_id, presence: true, allow_blank: false
  validates :subscriber_id, presence: true, allow_blank: false
end

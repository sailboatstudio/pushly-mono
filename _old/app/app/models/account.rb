class Account < ActiveRecord::Base
  resourcify

  belongs_to :user
  has_many :campaigns, :dependent => :destroy
  has_many :lists, :dependent => :destroy
  has_many :conversations

  after_create :generate_friendly_name
  after_create :create_twilio_account
  # after_save :buy_number

  validates :name,
    presence: true
  # validates :friendly_name,
  #   presence: true,
  #   uniqueness: true
  # validates :sid,
  #   presence: true,
  #   uniqueness: true
  # validates :user_id,
  #   presence: true

  extend FriendlyId
    friendly_id :uuid, use: [:slugged, :finders]

  def self.find_numbers(area_code)
    area_code = area_code || ""
    @twilio = Twilio::REST::Client.new
    numbers = @twilio.available_phone_numbers.get('CA').local.list(
      contains: area_code
    )

    arr = []
    numbers.each {|num| arr << num.phone_number}
    return arr
  end


  def self.generate_uuid
    RandomWord.adjs.next + '-' + RandomWord.adjs.next + '-' + RandomWord.nouns.next
  end

  def subscribers
    list_ids = self.lists.pluck(:id)
    Subscription.where(list_id: list_ids)
  end

  def buy_number
    if self.phone_number_purchased == false
      puts "run the purchase sequence"

      @twilio = Twilio::REST::Client.new
      account = @twilio.accounts.get(self.sid)
      if @client.incoming_phone_numbers.create(phone_number: phone_number)
        phone_number_purchased = true
        save
      else
        puts "there was an error in the twilio connection / account creation"
      end
    else
      puts "no purchase was made. The account has a purchased phone number."
    end
  end

  def auth_token
    @twilio = Twilio::REST::Client.new
    @twilio.accounts.get(self.sid).auth_token
  end

  private

  def generate_hash
    SecureRandom.hex
  end

  def generate_friendly_name
    self.friendly_name = "pushly-" + SecureRandom.hex(5) + "-" + self.name[0..45]
    self.save
  end

  def create_twilio_account
    @twilio = Twilio::REST::Client.new
    twilio_account = @twilio.accounts.create(:friendly_name => self.friendly_name)
    self.sid = twilio_account.sid
    self.save
  end
end

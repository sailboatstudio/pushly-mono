class Report < ActiveRecord::Base
  belongs_to :subscriber
  belongs_to :list
  belongs_to :account
end

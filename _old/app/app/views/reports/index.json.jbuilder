json.array!(@reports) do |report|
  json.extract! report, :id, :name, :phone_number, :email, :description, :subscriber_id, :list_id, :account_id, :status
  json.url report_url(report, format: :json)
end

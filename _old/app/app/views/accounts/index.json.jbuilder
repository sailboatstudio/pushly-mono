json.array!(@accounts) do |account|
  json.extract! account, :id, :name, :friendly_name, :sid, :description, :phone_number, :user_id
  json.url account_url(account, format: :json)
end

json.conversations @conversations.each do |conversation|
  json.(conversation, :id, :status, :created_at, :updated_at)

  json.subscriber do
    json.id conversation.subscriber.id
    json.name conversation.subscriber.name
    json.phone_number conversation.subscriber.phone_number
    json.bio conversation.subscriber.bio
    json.phone_number_hashed conversation.subscriber.phone_number_hashed
  end

  json.account do
    json.id conversation.account.id
    json.name conversation.account.name
    json.friendly_name conversation.account.friendly_name
    json.sid conversation.account.sid
    json.description conversation.account.description
    json.phone_number conversation.account.phone_number
    json.uuid conversation.account.uuid
    json.slug conversation.account.slug
  end

  # json.messages conversation.messages.each do |message|
  #   json.(message, :to, :from, :account_id, :subscriber_id, :sent_by, :created_at, :updated_at, :sid, :campaign_id, :message)
  # end
end

json.array!(@campaigns) do |campaign|
  json.extract! campaign, :id, :account_id, :created_by, :name, :description, :message
  json.url campaign_url(campaign, format: :json)
end

window.Pushly = {
  init: function() {},
  checkNotificationAbility: function() {
    if ('Notification' in window) {
      Pushly.notifyAbility = true;
    }
  },
  setCurrentRoom: function() {
    fetchConversation(function(){
      MD5(Pushly.current_conversation.id.toString(), function(str) {
        Pushly.current_room = str;
      });
    });

    Pushly.obj.chatWrapper = $('.js-convo-outer');
    Pushly.obj.chatInner = $('.js-convo-inner');
  },
  current_room: "",
  current_conversation: {},
  rooms: [],
  nofityAbility: false,

  obj: {
    chatWrapper: "",
    chatInner: ""
  }
};

// Pushly.io = io('http://bridge.pushly.co');
Pushly.io = io('localhost:5000');

fetchConversations = function() {
  $.ajax({
    url: window.location.protocol + "//" + window.location.host + window.location.pathname + '.json',
    method: "GET",
    success: function(res) {
      res.conversations.forEach(function (data, index) {
        Pushly.rooms.push(data);
        console.log("converastions updates");
      });
    }
  });
}

fetchConversation = function(callback) {
  $.ajax({
    url: window.location.protocol + "//" + window.location.host + window.location.pathname + '.json',
    method: "GET",
    success: function(res) {
      Pushly.current_conversation = res;
      console.log("current converastion updated");

      if (callback && typeof(callback) === "function") {
        callback(res);
      }
    }
  });
}


MD5 = function(str, callback) {
  $.ajax({
    url: window.location.protocol + "//" + window.location.host + '/md5.json?string=' + str,
    method: "POST",
    success: function(res) {
      if (callback && typeof(callback) === "function") {
        callback(res);
      }
    }
  });
}


//
// convoResponse = function(div, form, form-input) {
//   this.div  = div;
//   this.form = form;
//   this.form-input = form;
// }
//
// convoResponse.start = function() {
//   // Start running functions A & B
// }
//
// convoResponse.stop = function() {
//   // Stop running functions A &B
// }
//
// convoResponse.submit = function() [
//   // Run A & B, and then submit form
// ]
//
// var r = new convoResponse($('.js-convo-response'), )

// When someone types we want to:
// |
// |____ Run a function that does the follow
// |
// |____ A: Check if that person's message is greater than 160characters and split that visually.
// |
// |____ B: Copy the content .text() into the hidden form & remove anything we added in.
// |
// |____ When they hit enter, submit the form. When they hit Shift-Enter do a line break.

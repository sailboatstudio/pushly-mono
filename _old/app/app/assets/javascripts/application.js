//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require socket-io.min
//= require pushly
//= require turbolinks
//= require semantic-ui
//= require react
//= require react_ujs
//= require components

$(document).ready(function() {

  $(".js-account-phoneSelect").click(function() {
    $(".js-account-phoneSelect-input").val($(this).data("number"));
  });

  $('.ui.dropdown').dropdown();

  $('.js-convo-message').popup();

  $('.message.fadeout').delay(4000).fadeOut(400);
  $('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade');
  })
});

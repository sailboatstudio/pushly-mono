class SubscriptionsController < ApplicationController
  def create
    @subscription = Subscription.new(subscription_params)
    if @subscription.save
      redirect_to :back
    end
  end

  private

    def subscription_params
      params.require(:subscription).permit(:list_id, :subscriber_id)
    end
end

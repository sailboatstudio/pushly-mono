class ListsController < ApplicationController
  load_and_authorize_resource
  before_filter :authenticate_user!
  before_action :set_account

  def index
    @lists = @account.lists
  end

  def show
    @subscribers = @list.subscribers
  end

  def new
    @list = List.new
  end

  def edit
  end

  def create
    @list = List.new(list_params)
    @list.account_id = @account.id

    respond_to do |format|
      if @list.save
        format.html { redirect_to account_list_path(@account, @list), notice: 'List was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to account_list_path(@account, @list), notice: 'List was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @list.destroy
    respond_to do |format|
      format.html { redirect_to account_lists_url(@account), notice: 'List was successfully destroyed.' }
    end
  end

  private
    def set_account
      @account = Account.find(params[:account_id])
    end

    def list_params
      params.require(:list).permit(:name, :description)
    end
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :set_user, except: [:generate_md5]


  def generate_md5
    if params[:string]
      md5 = Digest::MD5.hexdigest(params[:string])
      render plain: "#{md5}"
    end
  end

  protected

  def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :username, :phone_number, :email, :password, :password_confirmation) }
      # devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :current_password, :is_female, :date_of_birth) }
  end

  def set_user
    @user = current_user if current_user
  end
end

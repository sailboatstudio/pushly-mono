class ConversationsController < ApplicationController
  before_filter :set_account
  before_filter :fetch_conversations
  before_filter :authenticate_user!


  def index
  end

  def show
    @conversation = Conversation.find(params[:id])
    @messages = @conversation.messages.where(hidden: false)
    authorize! :read, @conversation
  end

  private

  def fetch_conversations
    @conversations = Conversation.accessible_by(current_ability).available.order('updated_at DESC')
  end

  def set_account
    @account = Account.find(params[:account_id])
  end
end

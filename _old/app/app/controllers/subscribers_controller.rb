class SubscribersController < ApplicationController
  # load_and_authorize_resource
  before_filter :authenticate_user!, except: [:manage]
  before_filter :find_by_hash, only: [:manage]

  def index
    @subscribers = Subscriber.all
  end

  def show
    @subscriber = Subscriber.find(params[:id])
    @subscription = @subscriber.subscriptions.build
  end

  def manage
    if @subscriber.check_manage_token(params[:manage_token])
      @subscriptions = @subscriber.subscriptions
    else
      redirect_to root_url, notice: 'This is not a valid link. It may have expired. Try texting MANAGE again. :)'
    end
  end

  def new
    @subscriber = Subscriber.new
  end

  def edit
  end

  def create
    @subscriber = Subscriber.new(subscriber_params)

    respond_to do |format|
      if @subscriber.save
        format.html { redirect_to @subscriber, notice: 'Subscriber was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @subscriber.update(subscriber_params)
        format.html { redirect_to @subscriber, notice: 'Subscriber was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @subscriber.destroy
    respond_to do |format|
      format.html { redirect_to subscribers_url, notice: 'Subscriber was successfully destroyed.' }
    end
  end

  private
    def find_by_hash
      @subscriber = Subscriber.where(phone_number_hashed: params[:phone_hash]).limit(1).first
    end

    def subscriber_params
      params.require(:subscriber).permit(:name, :bio, :phone_number, :phone_number_hashed, :manage_token)
    end
end

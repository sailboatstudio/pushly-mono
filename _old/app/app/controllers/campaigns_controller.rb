class CampaignsController < ApplicationController
  load_and_authorize_resource

  before_filter :authenticate_user!
  before_action :set_account

  def index
    @campaigns = @account.campaigns
  end

  def show
  end

  def new
    @campaign = Campaign.new
  end

  def edit
    # if @campaign.sent_on != nil
    #   redirect_to :back, notice: 'Campaign has already been sent, you can\'t edit this.'
    # end
  end

  def create
    @campaign = Campaign.new(campaign_params)
    @campaign.account_id = @account.id

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to [@account, @campaign], notice: 'Campaign was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    if @campaign.sent?
      redirect_to edit_account_campaign_path(@account, @campaign), notice: 'This has been sent. You can\'t change it anymore.'
    else
      respond_to do |format|
        if @campaign.update(campaign_params)
          format.html { redirect_to edit_account_campaign_path(@account, @campaign), notice: 'Campaign was successfully updated.' }
        else
          format.html { render :edit }
        end
      end
    end
  end

  def destroy
    @campaign.destroy
    respond_to do |format|
      format.html { redirect_to account_campaigns_url(@account), notice: 'Campaign was successfully destroyed.' }
    end
  end


  def send_it
    respond_to do |format|
      if @campaign.send_it
        format.html { redirect_to @account, notice: 'It\'s been sent!' }
      else
        format.html { render :show }
      end
    end
  end


  private
    def set_account
      @account = Account.friendly.find(params[:account_id])
    end

    def campaign_params
      params.require(:campaign).permit(:account_id, :list_id, :name, :description, :message)
    end
end

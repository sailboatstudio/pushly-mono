class MessagesController < ApplicationController
  protect_from_forgery :except => [:incoming]

  def new
    @account = Account.find(params[:account_id])
    @message = Message.new
  end

  def quicksend_create
    @message = Message.new(message_params.merge(
      sent_by:    "account",
      from:       Account.find(message_params[:account_id]).phone_number,
      to:         Subscriber.find(message_params[:subscriber_id]).phone_number
    ))

    # respond_to do |format|
      if @message.save
        # format.html { redirect_to account_path(@message.account_id), notice: 'Messages Sent!' }
        # format.json { render status: 200 }
        render plain: '200'
      else
        # format.html { render :new }
        # format.json { render status: 500 }
        render plain: '500'
      end
    # end
  end

  def create
    @message = Message.create(message_params)
  end

  def incoming
    ## Initialize Message Object
    @message = Message.new(
      sent_by: 'subscriber',
      sid: params['SmsSid'],
      message: params['Body'],
      to: params['To'],
      from: params['From'],
    )

    ## Find subscriber
    @subscriber = Subscriber.where(phone_number: params['From'])
    ## Find Account
    @account = Account.where(phone_number: params['To']).limit(1).first
    ## Get Pushly Account
    @pushly = Account.where(sid: ENV['PUSHLY_SID']).limit(1).first

    ## Number Exists in Pushly
    if @subscriber.first
      @subscriber = @subscriber.first

      case @subscriber.status
      when 0 #Incomplete
        # Check for Flows, if name completed -> activate subscriber && @message.check_context
      when 1 #Active
        @context = @message.check_context(respond: true)
        @message.context = @context
      when 2 #Blocked
        @subscriber.send_account_blocked_message
        @context = @message.check_context
        @message.context = @context
      end

    ## Number is new to Pushly
    else
      @subscriber = Subscriber.create( phone_number: @message.from )
      @subscriber.subscriptions.create( list_id: @pushly.lists.first.id, active: true )
      ## Start Subscriber Name Flow
    end

    @message.subscriber_id = @subscriber.id
    @subscriber.subscribed_to_account?(@account)

    if @message.save!
      head 201, content_type: "text/html"
      puts @message
    else
      head 500, content_type: "text/html"
    end
  end

  private

  def message_params
    params.require(:message).permit(:sent_by, :message, :account_id, :subscriber_id, :campaign_id, :to, :from)
  end
end

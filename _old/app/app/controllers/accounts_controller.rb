class AccountsController < ApplicationController
  before_filter :authenticate_user!

  load_and_authorize_resource
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  layout "account", only: [:show, :new, :create, :edit, :update, :destroy]
  layout "select", only: :index

  def index
    @accounts = @user.accounts

    if @accounts.count == 1
      redirect_to @accounts.first
    end
  end

  def show
  end

  def new
    @numbers = Account.find_numbers("778")
    @accounts = @user.accounts
  end

  def edit
    if @account.phone_number == nil
      @numbers = Account.find_numbers("778")
    else
      @numbers = []
    end
  end

  def create
    @account.user_id = @user.id
    @account.uuid = Account.generate_uuid

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    acc = @twilio.accounts.get(@account.sid)
    acc.update(status: "closed")

    if acc.status === "closed"
      @account.destroy
      respond_to do |format|
        format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      raise "Unable to close Twilio Account"
    end
  end

  private
    def set_account
      @account = Account.find(params[:id])
    end

    def account_params
      params.require(:account).permit(:name, :friendly_name, :sid, :description, :phone_number, :user_id)
    end
end

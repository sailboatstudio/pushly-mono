# Pushly Web App

## To Do

- [ ] Back Button (instead of '+')
- [ ] Layout for external application actions. (Reports, Sessions, Subscriber)
- [ ] Report Webforms -> Via manage?
- [ ] List Webforms & Embeds
- [ ] Flows, Steps, Data Saves
- [ ] Account On-Boarding
  - [ ] Area Code Select
  - [ ] Purchase Numbers
  - [ ] Business Name Attributes
- [ ] Terms of Service & Privacy Policy
- [ ] Website Landing Page & Help Section
- [ ] Clear DB, ReSeed
- [ ] User & Account Edit Views
- [ ] Billing


## Future Features
- [ ] Deals/Discounts
- [ ] Custom Quick-Respond Messages
- [ ] Custom Account flows (hours, etc.)
- [ ] Account Multiple Users & Roles
- [ ]

var express = require('express'),
    redis   = require('redis'),
    client  = redis.createClient({url: process.env.PUSHLY_REDIS_URL || ''}),
    app     = express(),
    http    = require('http').Server(express),
    io      = require('socket.io')(http);
    // bridge  = express();
    // app     = express(),


/**********************
 *
 *
 **********************/

// io.on('connection', function(socket){
  // client.publish("user:connected")
// });

client.subscribe('messages:new');

client.on("message", function (channel, message) {
  console.log(channel, message);
  io.emit(channel, message);
});

client.on("error", function (err) {
  console.log("Error " + err);
});


// bridge.set('port', (process.env.PORT || 5000));
app.get('/', function(request, response) {
  response.sendStatus(200).end();
});
// bridge.listen(bridge.get('port'), function() {
//   console.log('Node app is running on port', bridge.get('port'));
// });

http.listen((process.env.PORT || 5000), function(){
  console.log('listening on *:' + (process.env.PORT || 5000));
});

import ApiClient from './ApiClient';

class PushlyApi extends ApiClient {
  constructor(config) {
    super(config);

    this.user = {

      login(payload): super.createEndpoint(payload, {
        endpoint: 'users/sessions',
        method: 'POST',
        payloadValidation: false
      }),

      refreshAuthToken(payload): super.createEndpoint(payload, {
        endpoint: 'users/sessions/refresh',
        method: 'POST',
        payloadValidation: false
      })

    }
  }
}


export default const Api = new PushlyApi({
  version: 'v1',
  env: 'development',
});

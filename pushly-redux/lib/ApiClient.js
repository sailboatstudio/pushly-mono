class ApiClient {
  constructor(config) {
    this.version        = config.version        || 'v1';
    this.env            = config.env            || 'development';
    this.developmentUrl = config.developmentUrl || 'http://localhost:3000';
    this.stagingUrl     = config.stagingUrl     || 'https://api.staging.pushly.co';
    this.productionUrl  = config.productionUrl  || 'https://api.pushly.co';


    this.baseUrl = switch (this.env) {
      case 'production':
        return this.productionUrl;
        break;
      case 'staging':
        return this.stagingUrl;
        break;
      case 'development':
      default:
        return this.developmentUrl;
        break;
    }

    this._validatePayload = this._validatePayload.bind(this);
  }

  createEndpoint(payload, config) {
    var validated = true

    if ( config.payloadValidation !== false && config.payloadValidation !== undefined) {
      validated = _validatePayload( config.payloadValidations )
    }

    if (this.env === mock) {

    }


    // Check if validated, then build API call on promise
      // Ajax({
      //   url: this.baseUrl + config.endpoint,
      //   method: payload.method,
      // })

    // Convert JSON from Promise to Javascript Object

  }

  _validatePayload( validations ) {
    validations.map( ( value, index, array ) => {
      if ( value.validations.required ) {
        // ( value.data ? true : false )
      }

      if ( value.validations.typeOf !== undefined) {
        // ( value.data.typeOf( value.validations.typeOf ) ? true : false )
      }

    });
  }

}

export default ApiClient;

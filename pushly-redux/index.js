// import * as reducer from './reducers';
// import * as lib from './lib';
// import * as actions from './actions';


import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import baseReducer from './reducers';

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
  );
  return createStore(baseReducer, initialState, enhancer);
};

export const store = configureStore({
  user: {}
});

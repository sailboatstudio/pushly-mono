import * as UserActions from './user.actions';

export const ActionCreators = Object.assign({},
  UserActions,
);

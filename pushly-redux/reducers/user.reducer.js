import * as types from '../actions/types';

export const user = function(state = {}, action) {
  switch(action.type) {
    case types.SET_USER:
      return {
        ...state,
        name: action.payload.name,
        username: action.payload.username,
        authToken: action.payload.authToken,
        refreshToken: action.payload.refreshToken,
      }
      break;
    default:
      return state;
  }

}

import { combineReducers } from 'redux';
import * as userReducers from './user.reducer';

export default combineReducers(Object.assign(
  userReducers
))

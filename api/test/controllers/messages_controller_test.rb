require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @message = messages(:one)
  end

  test "should get index" do
    get messages_url, as: :json
    assert_response :success
  end

  test "should create message" do
    assert_difference('Message.count') do
      post messages_url, params: { message: { account_id: @message.account_id, author: @message.author, campaign_id: @message.campaign_id, conversation_id: @message.conversation_id, from: @message.from, message: @message.message, person_id: @message.person_id, to: @message.to, twilio_id: @message.twilio_id } }, as: :json
    end

    assert_response 201
  end

  test "should show message" do
    get message_url(@message), as: :json
    assert_response :success
  end

  test "should update message" do
    patch message_url(@message), params: { message: { account_id: @message.account_id, author: @message.author, campaign_id: @message.campaign_id, conversation_id: @message.conversation_id, from: @message.from, message: @message.message, person_id: @message.person_id, to: @message.to, twilio_id: @message.twilio_id } }, as: :json
    assert_response 200
  end

  test "should destroy message" do
    assert_difference('Message.count', -1) do
      delete message_url(@message), as: :json
    end

    assert_response 204
  end
end

json.extract! account, :id, :name, :twilio_name, :twilio_id, :description, :phone_number, :uuid, :user_id, :created_at, :updated_at
json.url account_url(account, format: :json)

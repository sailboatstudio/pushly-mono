json.extract! campaign, :id, :name, :message, :sent_at, :list_id, :account_id, :created_at, :updated_at
json.url campaign_url(campaign, format: :json)

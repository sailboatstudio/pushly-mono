json.extract! list, :id, :name, :description, :account_id, :created_at, :updated_at
json.url list_url(list, format: :json)

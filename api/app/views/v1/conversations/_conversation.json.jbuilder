json.extract! conversation, :id, :subscription_id, :account_id, :person_id, :created_at, :updated_at
json.url conversation_url(conversation, format: :json)

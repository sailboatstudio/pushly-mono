json.extract! person, :id, :name, :phone_number, :phone_number_hashed, :created_at, :updated_at
json.url person_url(person, format: :json)

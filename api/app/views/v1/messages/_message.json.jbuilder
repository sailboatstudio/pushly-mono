json.extract! message, :id, :message, :to, :from, :author, :campaign_id, :conversation_id, :person_id, :account_id, :twilio_id, :created_at, :updated_at
json.url message_url(message, format: :json)

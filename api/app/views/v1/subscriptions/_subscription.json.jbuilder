json.extract! subscription, :id, :person_id, :list_id, :created_by, :created_at, :updated_at
json.url subscription_url(subscription, format: :json)

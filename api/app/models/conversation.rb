class Conversation < ApplicationRecord
  belongs_to :subscription
  belongs_to :account
  belongs_to :person
end

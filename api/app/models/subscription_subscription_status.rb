class SubscriptionSubscriptionStatus < ApplicationRecord
  belongs_to :subscription
  belongs_to :subscription_status
end

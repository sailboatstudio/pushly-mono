class Message < ApplicationRecord
  belongs_to :campaign
  belongs_to :conversation
  belongs_to :person
  belongs_to :account
end

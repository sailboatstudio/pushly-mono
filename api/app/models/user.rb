class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable

  has_many :tokens,
    dependent: :destroy,
    as: :owner,
    inverse_of: :owner


  def name
    return self.first_name + " " + self.last_name
  end

  def refresh_token
    self.tokens.where(active: true, type: "refresh").last
  end

  def find_or_create_refresh_token
    if !self.refresh_token
      self.create_refresh_token
    end

    self.refresh_token
  end

  def create_refresh_token
    Token.create_token_by_type("refresh", self)
  end
end

class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.references :person, foreign_key: true
      t.references :list, foreign_key: true
      t.string :created_by

      t.timestamps
    end
  end
end

class CreateSubscriptionSubscriptionStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :subscription_subscription_statuses do |t|
      t.references :subscription, foreign_key: true
      t.references :subscription_status, foreign_key: true, index: { :name => "idx_subscription_subscription_statuses_on_subscription_status" }

      t.timestamps
    end
  end
end

class CreateCampaigns < ActiveRecord::Migration[5.0]
  def change
    create_table :campaigns do |t|
      t.string :name
      t.text :message
      t.datetime :sent_at
      t.references :list, foreign_key: true
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end

class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :message
      t.string :to
      t.string :from
      t.string :author
      t.references :campaign, foreign_key: true
      t.references :conversation, foreign_key: true
      t.references :person, foreign_key: true
      t.references :account, foreign_key: true
      t.string :twilio_id

      t.timestamps
    end
  end
end

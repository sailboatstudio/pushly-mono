class CreatePersonPersonStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :person_person_statuses do |t|
      t.references :person, foreign_key: true
      t.references :person_status, foreign_key: true

      t.timestamps
    end
  end
end

class CreateSubscriptionStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :subscription_statuses do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

class CreatePersonStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :person_statuses do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

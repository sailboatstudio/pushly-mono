class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :name
      t.string :phone_number
      t.string :phone_number_hashed

      t.timestamps
    end
  end
end

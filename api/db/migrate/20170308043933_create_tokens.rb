class CreateTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :tokens do |t|
      t.string :token
      t.string :token_type
      t.boolean :active
      t.references :owner, polymorphic: true

      t.timestamps
    end
  end
end

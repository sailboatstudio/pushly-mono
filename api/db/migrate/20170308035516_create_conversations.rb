class CreateConversations < ActiveRecord::Migration[5.0]
  def change
    create_table :conversations do |t|
      t.references :subscription, foreign_key: true
      t.references :account, foreign_key: true
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end

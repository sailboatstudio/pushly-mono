class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :twilio_name
      t.string :twilio_id
      t.text :description
      t.string :phone_number
      t.string :uuid
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :accounts, :uuid, unique: true
  end
end

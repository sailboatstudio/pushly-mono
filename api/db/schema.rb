# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170308043933) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.string   "twilio_name"
    t.string   "twilio_id"
    t.text     "description"
    t.string   "phone_number"
    t.string   "uuid"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id"], name: "index_accounts_on_user_id", using: :btree
    t.index ["uuid"], name: "index_accounts_on_uuid", unique: true, using: :btree
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "name"
    t.text     "message"
    t.datetime "sent_at"
    t.integer  "list_id"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_campaigns_on_account_id", using: :btree
    t.index ["list_id"], name: "index_campaigns_on_list_id", using: :btree
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "subscription_id"
    t.integer  "account_id"
    t.integer  "person_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["account_id"], name: "index_conversations_on_account_id", using: :btree
    t.index ["person_id"], name: "index_conversations_on_person_id", using: :btree
    t.index ["subscription_id"], name: "index_conversations_on_subscription_id", using: :btree
  end

  create_table "lists", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "account_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["account_id"], name: "index_lists_on_account_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.text     "message"
    t.string   "to"
    t.string   "from"
    t.string   "author"
    t.integer  "campaign_id"
    t.integer  "conversation_id"
    t.integer  "person_id"
    t.integer  "account_id"
    t.string   "twilio_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["account_id"], name: "index_messages_on_account_id", using: :btree
    t.index ["campaign_id"], name: "index_messages_on_campaign_id", using: :btree
    t.index ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
    t.index ["person_id"], name: "index_messages_on_person_id", using: :btree
  end

  create_table "people", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "phone_number_hashed"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "person_person_statuses", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "person_status_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["person_id"], name: "index_person_person_statuses_on_person_id", using: :btree
    t.index ["person_status_id"], name: "index_person_person_statuses_on_person_status_id", using: :btree
  end

  create_table "person_statuses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "subscription_statuses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "subscription_subscription_statuses", force: :cascade do |t|
    t.integer  "subscription_id"
    t.integer  "subscription_status_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["subscription_id"], name: "index_subscription_subscription_statuses_on_subscription_id", using: :btree
    t.index ["subscription_status_id"], name: "idx_subscription_subscription_statuses_on_subscription_status", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "list_id"
    t.string   "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["list_id"], name: "index_subscriptions_on_list_id", using: :btree
    t.index ["person_id"], name: "index_subscriptions_on_person_id", using: :btree
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "token"
    t.string   "token_type"
    t.boolean  "active"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_tokens_on_owner_type_and_owner_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "campaigns", "accounts"
  add_foreign_key "campaigns", "lists"
  add_foreign_key "conversations", "accounts"
  add_foreign_key "conversations", "people"
  add_foreign_key "conversations", "subscriptions"
  add_foreign_key "lists", "accounts"
  add_foreign_key "messages", "accounts"
  add_foreign_key "messages", "campaigns"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "people"
  add_foreign_key "person_person_statuses", "people"
  add_foreign_key "person_person_statuses", "person_statuses"
  add_foreign_key "subscription_subscription_statuses", "subscription_statuses"
  add_foreign_key "subscription_subscription_statuses", "subscriptions"
  add_foreign_key "subscriptions", "lists"
  add_foreign_key "subscriptions", "people"
end

Rails.application.routes.draw do
  ####################################
  # Version 1.x.x API

  namespace :v1, defaults: { format: 'json' } do

    ## Authentication
      devise_for :users,
        path_names: {
          sign_in: 'sessions',
          sign_out: 'sessions'
        },
        controllers: {
          sessions: 'v1/users/sessions',
          registrations: 'v1/users/registrations'
        }
      post 'users/sessions/refresh', to: 'v1/application#refresh', as: 'refresh_user_session'

    ## Scaffolded Resources
      resources :messages
      resources :campaigns
      resources :conversations
      resources :subscriptions
      resources :people
      resources :lists
      resources :accounts

    ## Test
      get 'test', to: 'v1/application#test'


  end

  # END 1.x.x
  ####################################
end
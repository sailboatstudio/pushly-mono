import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

const DEFAULT_PROPS = {
  iconColor: '#FFFFFF',
  iconBackground: '#A2A2A2',
  titleColor: '#454545',
  subtitleColor: '#A2A2A2',
  metaColor: '#A2A2A2',
}

export default class UIListItem extends Component {
  static propTypes = {
    icon: React.PropTypes.string,
    iconColor: React.PropTypes.string,
    iconBackground: React.PropTypes.string,
    iconStyle: React.PropTypes.object,
    iconWrapperStyle: React.PropTypes.object,

    title: React.PropTypes.string.isRequired, // Required
    titleColor: React.PropTypes.string,
    titleStyle: React.PropTypes.object,

    subtitle: React.PropTypes.string,
    subtitleColor: React.PropTypes.string,
    subtitleStyle: React.PropTypes.object,

    contentWrapperStyle: React.PropTypes.object,

    meta: React.PropTypes.string,
    metaColor: React.PropTypes.string,
    metaStyle: React.PropTypes.object,
    metaWrapperStyle: React.PropTypes.object,

    unseen: React.PropTypes.bool,
    onPress: React.PropTypes.func,
    onLongPress: React.PropTypes.func,
  }

  static defaultProps = DEFAULT_PROPS;

  constructor() {
    super();

    this.renderIcon = this.renderIcon.bind(this);
    this.renderTitle = this.renderTitle.bind(this);
    this.renderSubtitle = this.renderSubtitle.bind(this);
    this.renderMeta = this.renderMeta.bind(this);
  }

  renderIcon() {
    if (!!this.props.icon) {
      return (
        <View style={[styles.iconWrapper, this.props.iconWrapperStyle]}>
          <Text style={[styles.iconText, this.props.iconStyle]}>
            {this.props.icon}
          </Text>
        </View>
      )
    }
  }

  renderTitle() {
    return (
      <Text style={[
        styles.contentTitleText,
        this.props.titleStyle,
        (!this.props.unseen ? unseenStyles.contentTitleText : null)
      ]}>
        {this.props.title}
      </Text>
    )
  }

  renderSubtitle() {
    if (!!this.props.subtitle) {
      return (
        <Text style={[
          styles.contentSubtitleText,
          this.props.subtitleStyle,
          (!this.props.unseen ? unseenStyles.contentSubtitleText : null)
        ]}>
          {this.props.subtitle}
        </Text>
      )
    }
  }

  renderMeta() {
    if (!!this.props.meta) {
      return (
        <View style={[styles.metaWrapper, this.props.metaWrapperStyle]}>
          <Text style={[styles.metaText, this.props.metaStyle]}>
            {this.props.meta}
          </Text>
        </View>
      )
    }
  }

  render() {
    return(
      <TouchableOpacity
        onPress={this.props.onPress}
        onLongPress={this.props.onLongPress}
        style={[
          styles.itemWrapper,
          (!this.props.unseen ? unseenStyles.itemWrapper : null)
        ]}
      >
        { this.renderIcon() }

        <View style={[styles.contentWrapper, this.props.contentWrapperStyle]}>
          { this.renderTitle() }
          { this.renderSubtitle() }
        </View>

        { this.renderMeta() }
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    padding: 17,
    borderRadius: 7,
    shadowColor: '#000000',
    shadowOpacity: 0.10,
    shadowRadius: 10,
    shadowOffset: { x: 0, y: 0 }
    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    // backgroundColor: '#FFFFFF',
    // padding: 17,
    // borderBottomWidth: 1,
    // borderBottomColor: "rgba(0,0,0,0.15)",
    // borderStyle: 'solid',
  },
  iconWrapper: {},
  iconText: {},
  iconImage: {},
  contentWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  contentTitleText: {
    color: DEFAULT_PROPS.titleColor,
  },
  contentSubtitleText: {
    marginTop: 5,
    color: DEFAULT_PROPS.subtitleColor,
  },
  metaWrapper: {},
  metaText: {
    color: DEFAULT_PROPS.metaColor,
  },
});


// TODO: Proper unseen management
const unseenStyles = StyleSheet.create({
  itemWrapper: {
    borderLeftWidth: 2,
    borderLeftColor: "#FF3C3C",
  },
  contentTitleText: {
    fontWeight: '600',
  },
  contentSubtitleText: {
    fontWeight: '600',
  },
});

import React, { Component, PropTypes } from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';

import UITitle from './UITitle.component';

export default class UIBUtton extends Component {
  static propTypes = {
    label: PropTypes.string,
    buttonStyle: PropTypes.oneOf(['primary', 'secondary']),
    disabled: PropTypes.bool,

    wrapperStyle: PropTypes.object,
    labelStyle: PropTypes.object,

    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func,
  }

  static defaultProps = {
    label: "Continue",
    disabled: false,
    buttonStyle: 'secondary'
  }

  render() {
    return (
      <TouchableOpacity style={[
        styles.wrapper,
        (this.props.buttonStyle === 'primary' ? styles.wrapperPrimary : styles.wrapperSecondary),
        (this.props.wrapperStyle ? this.props.wrapperStyle : null)
      ]}>
        <Text style={[
          styles.label,
          (this.props.buttonStyle === 'primary' ? styles.labelPrimary : styles.labelSecondary),
          (this.props.labelStyle ? this.props.labelStyle : null)
        ]}>
          {this.props.label}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    margin: 10,
    marginTop: 15,
    padding: 16,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF3C3C',
    shadowColor: '#000000',
    shadowOpacity: 0.10,
    shadowRadius: 10,
    shadowOffset: { x: 0, y: 0 }
  },
  label: {
    color: '#FFFFFF',
    fontSize: 15,
  },
  wrapperPrimary: {},
  wrapperSecondary: {},
  labelPrimary: {},
  labelSecondary: {},
});

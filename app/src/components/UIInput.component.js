import React, { Component, PropTypes } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

import UITitle from './UITitle.component';

export default class UIInput extends Component {
  static propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,

    onChangeText: PropTypes.func.isRequired,
    selectionColor: PropTypes.string,
    keyboardAppearance: PropTypes.oneOf(['default', 'light', 'dark']),
    selectionColor: PropTypes.string,

    wrapperStyle: PropTypes.object,
    inputStyle: PropTypes.object,
  }

  static defaultProps = {
    placeholder: "Hi, I'm a placeholder",
    keyboardAppearance: 'dark',
    selectionColor: "#FF3C3C",
  }

  constructor(props) {
    super(props);

    this.renderTitle = this.renderTitle.bind(this);
  }

  renderTitle(title) {
    if (!!this.props.title) {
      return <UITitle title={title} />
    }
  }

  render() {
    return (
      <View style={[
        styles.container,
        (this.props.wrapperStyle ? this.props.wrapperStyle : null)
      ]}>
        { this.renderTitle(this.props.title) }
        <TextInput
          style={[
            styles.input,
            (this.props.inputStyle ? this.props.inputStyle : null)
          ]}
          multiline={true}
          placeholder={this.props.placeholder}
          keyboardAppearance={this.props.keyboardAppearance}
          selectionColor={this.props.selectionColor}
          onChangeText={(val) => this.props.onChangeText(val)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  input: {
    margin: 10,
    padding: 17,
    paddingTop: 7,
    borderRadius: 7,
    borderColor: "rgba(0,0,0,0.25)",
    borderWidth: 1,
    height: 100,
    fontSize: 15,
  },
});

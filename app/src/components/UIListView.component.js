import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
} from 'react-native';

import UIListItem from './UIListItem.component';

export default class UIListView extends Component {
  static propTypes = {
    title: React.PropTypes.string,
    theme: React.PropTypes.oneOf(["Light", "Dark"]),
    items: React.PropTypes.array,
  }

  static defaultProps = {}

  constructor(props) {
    super()

    this.renderRow = this.renderRow.bind(this);
    this.renderTitle = this.renderTitle.bind(this);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(props.items),
    }
  }

  renderTitle() {
    if (!!this.props.title) {
      return (
        <Text style={styles.titleText}>{this.props.title}</Text>
      );
    }
  }

  renderRow(rowData) {
    let lastMessage = rowData.messages[rowData.messages.length - 1];

    let title = rowData.recipients[0].name;
    let subtitle = lastMessage.text.substring(0,40);

    // TODO: get right datetime
    let meta = "14m" || lastMessage.createdAt;
    let unseen = !!lastMessage.readAt;

    return(
      <UIListItem
        title={title}
        subtitle={subtitle}
        meta={meta}

        unseen={unseen}
        onPress={() => this.props.navigation.navigate('Conversation', {id: rowData.id, conversation: rowData})}
        // onLongPress={}
      />
    )
  }


  render() {
    return (
      <View style={styles.container}>
        { this.renderTitle() }
        <ListView
          style={{paddingTop: 15}}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  titleText: {
    backgroundColor: 'transparent',
    margin: 20,
    marginLeft: 27,
    marginBottom: 0,
    fontSize: 24,
    fontWeight: "300",
  },
});

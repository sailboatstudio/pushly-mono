import React, {
  Component,
  PropTypes
} from 'react';

import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import UITitle from './UITitle.component';

export default class UISelect extends Component {
  static propTypes = { 
    title: PropTypes.string,
    items: PropTypes.array,
    required: PropTypes.bool,
    minSelect: PropTypes.number,
    maxSelect: PropTypes.number,
    toggle: PropTypes.bool,

    onSelectedChange: PropTypes.func.isRequired,
    itemOnPress: PropTypes.func,
    itemOnLongPress: PropTypes.func,

    actionItemLabel: PropTypes.string,
    actionItemOnPress: PropTypes.func,
    actionItemOnLongPress: PropTypes.func,

    wrapperStyle: PropTypes.object,
    titleWrapperStyle: PropTypes.object,
    titleTextStyle: PropTypes.object,
    itemsWrapperStyle: PropTypes.object,
    itemWrapperStyle: PropTypes.object,
    itemTextStyle: PropTypes.object,
  }

  static defaultProps = {
    minSelect: 1,
    maxSelect: 1,
    toggle: true,
  }

  constructor(props) {
    super(props);

    this.renderItems = this.renderItems.bind(this);
    this.renderTitle = this.renderTitle.bind(this);
    this.renderAction = this.renderAction.bind(this);
    this.updateSelected = this.updateSelected.bind(this);
  }

  updateSelected(item) {
    let updateItems = [];

    let numSelected = 0;
    this.props.items.map((i) => {
      let newItem = i;

      // TODO: (Bug): If 3 items selected. Removing one removes 2 items. Wy the fuck?!
      if (newItem.isSelected) { numSelected++ }
      if (newItem._id === item._id) { newItem.isSelected = !newItem.isSelected } // Update onPress'd to be selected.
      if (this.props.toggle && newItem._id != item._id) { newItem.isSelected = false } // Remove isSelected if toggle is true.
      if (numSelected == this.props.maxSelect && newItem._id != item._id) { newItem.isSelected = false }
      updateItems.push(newItem);
    });
    
    this.props.onSelectedChange(updateItems);
  }

  renderTitle(title) {
    if (!!this.props.title) {
      return <UITitle title={title} />
    }
  }

  renderItems(items, itemOnPress, itemOnLongPress) {
    return items.map((item) => {
      return (
        <TouchableOpacity
          key={item._id}
          onPress={() => {
            this.updateSelected(item);
            (itemOnPress ? itemOnPress(item) : null);
          }}
          onLongPress={() => (itemOnLongPress ? itemOnLongPress(item) : null)}
          style={[
            styles.itemWrapper,
            (item.isSelected ? selectedStyles.itemWrapper : null)
          ]}
        >
          <Text style={[
            styles.itemText,
            (item.isSelected ? selectedStyles.itemText : null)
          ]}>
            {item.title}
          </Text>
        </TouchableOpacity>
      )
    })
  }

  renderAction(actionItemLabel, actionItemOnPress, actionItemOnLongPress){
    if (!!this.props.actionItemLabel) {
      return(
        <TouchableOpacity
          onPress={() => (actionItemOnPress ? actionItemOnPress() : null )}
          onLongPress={() => (actionItemOnLongPress ? actionItemOnLongPress() : null )}
          style={styles.actionItemWrapper}
        >
          <Text style={styles.actionItemText}>{actionItemLabel}</Text>
        </TouchableOpacity>

      )
    }
  }

  render() {
    return (
      <View style={[
        styles.container,
        (this.props.wrapperStyle ? this.props.wrapperStyle : null)
      ]}>
        { this.renderTitle(this.props.title) }
        
        <ScrollView
          contentContainerStyle={styles.itemsWrapper}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        >
          { this.renderItems(this.props.items, this.props.itemOnPress, this.itemOnLongPress) }
          { this.renderAction(this.props.actionItemLabel, this.props.actionItemOnPress, this.actionItemOnLongPress) }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
  itemsWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 5,
  },
  itemWrapper: {
    margin: 5,
    padding: 17,
    backgroundColor: "#FFFFFF",
    borderRadius: 7,
    borderColor: "#FFFFFF",
    borderWidth: 2,
    shadowColor: '#000000',
    shadowOpacity: 0.10,
    shadowRadius: 10,
    shadowOffset: { x: 0, y: 0 }
  },
  itemText: {},
  actionItemWrapper: {
    margin: 5,
    padding: 17,
    backgroundColor: "#FFFFFF",
    borderRadius: 7,
    borderColor: "#FF3C3C",
    borderWidth: 1,
    opacity: 0.5,
  },
  actionItemText: {
    color: "#FF3C3C",
  },
});

const selectedStyles = StyleSheet.create({
  container: {},
  itemsWrapper: {},
  itemWrapper: {
    borderColor: '#FF3C3C',
  },
  itemText: {
    color: "#FF3C3C",
  },
})
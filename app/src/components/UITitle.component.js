import React, { Component, PropTypes } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class UITitle extends Component {
  static propTypes = { 
    title: PropTypes.string.isRequired,
    titleTextStyle: PropTypes.object,
  }

  render() {
    return (
      <Text style={[
        styles.titleText,
        (this.props.titleTextStyle ? this.props.titleTextStyle : null)
      ]}>
        {this.props.title}
      </Text>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 17,
    margin: 5,
    marginLeft: 27,
  },
});

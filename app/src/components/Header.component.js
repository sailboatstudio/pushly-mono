import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class Header extends Component {
  static propTypes = {
    title: React.PropTypes.string,
  }

  constructor(props) {
    super(props);

    this.renderTitle = this.renderTitle.bind(this);
    this.renderLogo = this.renderLogo.bind(this);
  }

  renderTitle(title) {
    return (
      <View style={styles.titleWrapper}>
        <Text style={styles.titleText}>
          {title}
        </Text>
      </View>
    )
  }

  renderLogo() {
    return(
      <View style={styles.imageWrapper}>
        <Image
          source={require('../assets/images/icon-black-pushly.png')}
          width={30}
          height={30}
        />
      </View>
    );
  }
  
  render() {
    return (!!this.props.title) ?
      this.renderTitle(this.props.title) :
      this.renderLogo();
  }
}

const styles = StyleSheet.create({
  imageWrapper: {
    flex: 1,
    marginTop: 13,
    alignItems: 'center',
  }
});

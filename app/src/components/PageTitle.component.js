import React, {
  Component,
  PropTypes,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class PageTitle extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Text style={styles.titleText}>{this.props.title}</Text>
    );
  }
}

const styles = StyleSheet.create({
  titleText: {
    backgroundColor: 'transparent',
    margin: 20,
    marginLeft: 27,
    marginBottom: 0,
    fontSize: 24,
    fontWeight: "300",
  },
});

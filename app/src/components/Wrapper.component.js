import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

export default class Wrapper extends Component {
  render() {
    return(
      <View style={styles.wrapper}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  }
});

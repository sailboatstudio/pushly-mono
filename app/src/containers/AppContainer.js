import React, { Component } from 'react';
import ConversationsScreen from './Conversations.screen';
import ConversationScreen from './Conversation.screen';
import ComposeScreen from './Compose.screen';
import SettingsScreen from './Settings.screen';
import Header from '../components/Header.component';

import {
  StackNavigator,
  TabNavigator
} from 'react-navigation'

const MainNavigation = TabNavigator({
  Conversations: { screen: ConversationsScreen },
  Compose: { screen: ComposeScreen },
  Settings: { screen: SettingsScreen }
}, {
  tabBarOptions: {
    activeTintColor: '#FF3C3C',
    showLabel: false,
    style: {
      backgroundColor: '#FFFFFF',
      borderTopWidth: 0,
    }
  }
});

export const AppContainer = StackNavigator({
  Main: { screen: MainNavigation },
  Conversation: {
    path: 'conversation/:id',
    screen: ConversationScreen,
  },
}, {
  navigationOptions: {
    headerMode: "screen",
    header: {
      title: <Header />,
      tintColor: "#FF3C3C",
      style: {
        backgroundColor: "#FFFFFF",
        borderBottomWidth: 0,
        shadowColor: 'transparent',
      }
    }
  }
});

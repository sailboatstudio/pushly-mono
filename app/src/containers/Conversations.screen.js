import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import mockData from '../lib/mockData.json';
import UIListView from '../components/UIListView.component';
import Wrapper from '../components/Wrapper.component';
import Header from '../components/Header.component';
import PageTitle from '../components/PageTitle.component';


export default class ConversationsScreen extends Component {
  static navigationOptions = {
    header: {
      title: "Hello",
    },
    tabBar: {
      label: 'Conversations',
      icon: ({ tintColor }) => (
        <Icon name="comments" size={30} color={tintColor} />
      ),
    }
  }

  render() {
    return (
      <Wrapper>
        <PageTitle title={"Conversations"} />
        <UIListView
          items={mockData['conversations']}
          navigation={this.props.navigation}
        />
      </Wrapper>
    );
  }
};

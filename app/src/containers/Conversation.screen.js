import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat';
import Header from '../components/Header.component';

export default class ConversationsScreen extends Component {
  static navigationOptions = {
    title: 'Name',
  }

  constructor(props) {
    super(props);

    console.log(props);

    this.state = { messages: props.navigation.state.params.conversation.messages };
    this.renderBubble = this.renderBubble.bind(this);
    this.renderSend = this.renderSend.bind(this);
    this.renderAvatar = this.renderAvatar.bind(this);
    this.onSend = this.onSend.bind(this);
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: styles.bubbleWrapperStyleRight,
        }}
      />
    );
  }

  renderSend(props) {
    return (
      <Send
        {...props}
        textStyle={styles.sendTextStyle}
      />
    );
  }

  renderAvatar() {
    return null;
  }

  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          renderBubble={this.renderBubble}
          renderSend={this.renderSend}
          // renderAvatar={this.renderAvatar}
          user={{
            _id: 2,
          }}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#FFFFFF",
  },
  sendTextStyle: {
    color: '#FF3C3C',
  },
  bubbleWrapperStyleRight: {
    backgroundColor: '#FF3C3C',
  },
})

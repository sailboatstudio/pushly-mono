import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class SettingsScreen extends Component {
  static navigationOptions = {
    title: 'Settings',
    tabBar: {
      label: 'Settings',
      icon: ({ tintColor }) => (
        <Icon name="cog" size={30} color={tintColor} />
      ),
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Hello, World</Text>
        <Text>I'm the SettingsScreen</Text>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFFFFF",
  }
})

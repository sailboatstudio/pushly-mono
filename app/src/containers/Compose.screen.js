import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import Wrapper from '../components/Wrapper.component';
import PageTitle from '../components/PageTitle.component';

import UISelect from '../components/UISelect.component';
import UIInput from '../components/UIInput.component';
import UIButton from '../components/UIButton.component';

export default class ComposeScreen extends Component {
  static navigationOptions = {
    title: 'Compose',
    tabBar: {
      label: 'Compose',
      icon: ({ tintColor }) => (
        <Icon name="plus" size={30} color={tintColor} />
      ),
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      items: [
        { _id: 1, title: "General", value: "general-val" },
        { _id: 2, title: "Locals", value: "local-val" },
        { _id: 3, title: "USA", value: "usa-val" },
        { _id: 4, title: "Canada", value: "can-val" }
      ],
    };
  }

  render() {
    return (
      <Wrapper>
        <PageTitle title="New Campaign" />

        <UISelect
          title="List"
          items={this.state.items}
          actionItemLabel="New"
          actionItemOnPress={(val) => console.log(val)}
          maxSelect={3}
          toggle={false}
          onSelectedChange={(items) => this.setState({items: items})}
        />

        <UIInput
          title="Message"
          placeholder="This is where you can write your message…"
          onChangeText={(val) => console.log('input value changed')}
        />

        <UIButton
          label={"Review before sending"}
          buttonType={"primary"}
          onPress={() => console.log('button pressed')}
        />
      </Wrapper>
    );
  }
};

const styles = StyleSheet.create({

});

import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { store } from 'pushly-redux';

import { AppContainer } from './containers/AppContainer';

export default class App extends Component {
  componentDidMount() {
    console.log(store.getState())
  }

  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
